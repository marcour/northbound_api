from models import Configuration, DeviceId, VoInfo, Data, Friendship
from datetime import datetime

from models.Trigger import Trigger


class Populate(object):
    def populate_conf(self):
        conf = Configuration()
        conf.device_command = "GET_PROXIMITY"
        conf.feature = "sensor"
        conf.flag = "flag_PROXIMITY"
        conf.name = "PROXIMITY"
        conf.permission = "friend"
        conf.schedulable = True
        conf.schedule = 0
        conf.type = "number"
        conf.put()

        conf = Configuration()
        conf.device_command = "GET_POSITION"
        conf.feature = "sensor"
        conf.flag = "flag_POSITION"
        conf.name = "POSITION"
        conf.permission = "friend"
        conf.schedulable = True
        conf.schedule = 0
        conf.type = "position"
        conf.put()

    def populate_data(self):
        data = Data()
        data.name = "POSITION"
        data.timestamp = datetime.now()
        data.type = "position"
        data.value = '{ "latitude": 39.342236, "longitude": 9.503421 }'
        data.accuracy = "21"
        data.altitude = "200"
        data.bearing = "190"
        data.speed = "250"
        data.put()

        data1 = Data()
        data1.name = "PROXIMITY"
        data1.timestamp = datetime.now()
        data1.type = "number"
        data1.value = '113'
        data1.accuracy = "21"
        data1.altitude = "200"
        data1.bearing = "190"
        data1.speed = "250"
        data1.put()

    def populate_device_id(self):
        device_id = DeviceId()
        device_id.reg_id = "put_here_your_regId"
        device_id.put()

    def populate_vo_info(self):
        vo_info = VoInfo()

        vo_info.brand = "brando"
        vo_info.b_mac = "none"
        vo_info.friend_key = "put_here_a_friend_key"
        vo_info.id_vo = "144"
        vo_info.location = "cagliari"
        vo_info.latitude = "39 e spiccioli"
        vo_info.longitude = "9 e rotti"
        vo_info.location_name = "university"
        vo_info.location_range = "50"
        vo_info.model = "marlon"
        vo_info.owner_key = "put_here_your_owner_key"
        vo_info.owner = "sono lo owner"
        vo_info.url = "www.app_engine_id.appspot.com"
        vo_info.w_mac = "wifi_mac"
        vo_info.put()

    def populate_trigger(self):
        trigg = Trigger()
        trigg.appId = "3"
        trigg.condition = "IN_RANGE"
        trigg.event = "SENSOR"
        trigg.feature = "POSITION"
        trigg.output = "%timestamp%"
        trigg.output_address = "www.lysis-me.appspot.com/entry"
        trigg.output_method = "POST"
        trigg.status = "true"
        trigg.value = "39.235039,9.102545;500"

    def populate_friendship(self):
        friend = Friendship()
        friend.brand = "marlon_brand"
        friend.friend_key = "lvjhdohbcdowhbcljwk2e44k"
        friend.id_VO = "318"
        friend.location = "mobile"
        friend.location_latitude = "39.0976"
        friend.location_longitude = "9.0364"
        friend.location_name = "mobile"
        friend.location_range = "50"
        friend.model = "ledom"
        friend.owner = "biancaneve@gmil.com"
        friend.relation_type = "SOR"
        friend.status = "ack"
        friend.url = "thirdleveldomain.appspot.com"
        friend.b_mac = "hcbswkuhc"
        friend.w_mac = "kvbdslvjhds"
        friend.put()

    def populate_all(self):
        self.populate_conf()
        self.populate_data()
        self.populate_device_id()
        self.populate_vo_info()
        self.populate_trigger()
        self.populate_friendship()
