#!/usr/bin/env python
#
# Copyright 2007 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import json

import webapp2
from handlers import SensorHandler, FriendListHandler, \
    SetTriggerHandler, UnsetTriggerHandler, StatusTriggerHandler, \
    SetScheduleHandler, GetScheduleHandler, SetActuatorHandler
from models import Data
from test_datastore.popolate import Populate

class MainHandler(webapp2.RequestHandler):

    def get(self):
        pop = Populate()
        pop.populate_all()

        self.response.write('Hello world!')


from models.VoInfo import VoInfo




app = webapp2.WSGIApplication([
    ('/', MainHandler),
    ('/sensor', SensorHandler),
    ('/getFriendList', FriendListHandler),
    ('/setActuator', SetActuatorHandler),
    ('/trigger/set', SetTriggerHandler),
    ('/trigger/unset', UnsetTriggerHandler),
    ('/trigger/status', StatusTriggerHandler),
    ('/schedule/set', SetScheduleHandler),
    ('/schedule/get', GetScheduleHandler)

], debug=True)
