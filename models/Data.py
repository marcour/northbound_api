from google.appengine.ext import ndb


class Data(ndb.Model):
    name = ndb.StringProperty()
    timestamp = ndb.DateTimeProperty(auto_now=True)
    type = ndb.StringProperty()
    value = ndb.StringProperty()
    accuracy = ndb.StringProperty()
    altitude = ndb.StringProperty()
    bearing = ndb.StringProperty()
    speed = ndb.StringProperty()