from google.appengine.ext import ndb


class VoInfo(ndb.Model):
    brand = ndb.StringProperty()
    b_mac = ndb.StringProperty()
    friend_key = ndb.StringProperty()
    id_vo = ndb.StringProperty()
    location = ndb.StringProperty()
    latitude = ndb.StringProperty()
    longitude = ndb.StringProperty()
    location_name = ndb.StringProperty()
    location_range = ndb.StringProperty()
    model = ndb.StringProperty()
    owner_key = ndb.StringProperty()
    owner = ndb.StringProperty()
    url = ndb.StringProperty()
    w_mac = ndb.StringProperty()
