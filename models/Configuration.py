from google.appengine.ext import ndb


class Configuration(ndb.Model):
    device_command = ndb.StringProperty()
    feature = ndb.StringProperty()
    flag = ndb.StringProperty()
    name = ndb.StringProperty()
    permission = ndb.StringProperty()
    schedulable = ndb.BooleanProperty()
    schedule = ndb.IntegerProperty()
    type = ndb.StringProperty()

