from google.appengine.ext import ndb


class Friendship (ndb.Model):

    brand = ndb.StringProperty()
    friend_key = ndb.StringProperty()
    id_VO = ndb.StringProperty()
    location = ndb.StringProperty()
    location_latitude = ndb.StringProperty()
    location_longitude = ndb.StringProperty()
    location_name = ndb.StringProperty()
    location_range = ndb.StringProperty()
    model = ndb.StringProperty()
    owner = ndb.StringProperty()
    relation_type = ndb.StringProperty()
    status = ndb.StringProperty()
    url = ndb.StringProperty()
    b_mac = ndb.StringProperty()
    w_mac = ndb.StringProperty()
    timestamp = ndb.DateTimeProperty(auto_now=True)