import json

import webapp2

from handlers import RootHandler
from models import Configuration
from utils import get_grant_level


class GetScheduleHandler(RootHandler):

    def get_inputs(self):
        body = self.request.body
        self.bodydict = json.loads(body)
        self.mandatory = ["sensor_name"]
        self.check_inputs()

        sensor_name = self.bodydict['sensor_name']

        self.return_query = Configuration.query(
            Configuration.name == sensor_name).get()

    def granted_actions(self):
        schedule = self.return_query.schedule
        self.response.status_int = 200
        resp = json.dumps({"schedule_interval": schedule})
        self.response.write(resp)

