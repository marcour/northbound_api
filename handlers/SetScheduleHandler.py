import json

import webapp2

from handlers import RootHandler
from models import Configuration


class SetScheduleHandler(RootHandler):

    def get_inputs(self):
        body = self.request.body
        self.bodydict = json.loads(body)
        self.mandatory = ["sensor_name",
                          "schedule_interval"]
        self.check_inputs()
        sensor_name = self.bodydict['sensor_name']

        self.return_query = Configuration.query(
            Configuration.name == sensor_name).get()

    def granted_actions(self):
        self.return_query.schedule = int(self.bodydict["schedule_interval"])
        self.return_query.put()
        self.response.status_int = 200
        resp = json.dumps({"code": self.permission_code, "message": "everything is ok"})
        self.response.write(resp)
