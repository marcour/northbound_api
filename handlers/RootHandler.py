import json
import logging

import webapp2
from utils import get_grant_level

class RootHandler(webapp2.RequestHandler):

    def post(self):
        self.get_inputs()

        if self.return_query is None:
            logging.warning("RootHandler, post() method. Query result is empty.")
            webapp2.abort(400, detail="Bad Request, query result is empty")
            return

        try:
            permission = self.return_query.permission
        except Exception:
            permission = None

        if permission == "public":
            self.permission_code = 0
            self.granted_actions()

        elif permission == "friend":
            try:
                key = self.bodydict['key']
            except Exception:
                logging.warning("RootHandler, post() method. Missing key.")
                webapp2.abort(401, detail="The key is missing")
            grant = get_grant_level(key)

            if grant == "owner" or grant == "friend":
                if grant == "owner" :
                    self.permission_code = 2
                else:
                    self.permission_code = 1
                self.granted_actions()
            else:
                logging.warning("RootHandler, post() method. Not allowed with this key.")
                webapp2.abort(403, detail="Not Allowed")

        else:
            # default: only owner can access
            try:
                key = self.bodydict['key']
            except Exception:
                logging.warning("RootHandler, post() method. Missing key.")
                webapp2.abort(401, detail="The key is missing")
            grant = get_grant_level(key)
            if grant == "owner":
                self.permission_code = 2
                self.granted_actions()
            else:
                logging.warning("RootHandler, post() method. Not allowed with this key.")
                webapp2.abort(403, detail="Not Allowed")

    def check_inputs(self):
        """
        :param mandatory: list of mandatory parameters (as strings)
        """
        try:
            for i in self.mandatory:
                missing_parameter = i
                _ = self.bodydict[i]
        except Exception:
            logging.warning("RootHandler, check_inputs() method. Missing mandatory parameter: {}.".format(missing_parameter))
            webapp2.abort(400, detail="Bad Request, missing parameter '{}'".format(missing_parameter))

    def get_input(self):
        pass

    def granted_actions(self):
        pass