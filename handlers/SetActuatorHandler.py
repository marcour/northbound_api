import json
import logging

import webapp2

from handlers import RootHandler
from models import Configuration


class SetActuatorHandler(RootHandler):

    def get_input(self):

        body = self.request.body
        self.bodydict = json.loads(body)
        self.mandatory = ["actuator_name", "value"]
        self.check_inputs()

        actuator_name = self.bodydict['actuator_name']

        self.return_query = Configuration.query(
            Configuration.name == actuator_name).get()

        try:
            feat = self.return_query.feature
            if feat is not "actuator":
                logging.warning("SetActuatorHandler, get_input() method. Not actuator.")
                webapp2.abort(400, "Bad request. The resource is not an actuator.")
        except Exception:
            logging.warning(
                "SetActuatorHandler, get_input() method. Missing feature parameter.")
            webapp2.abort(400,"Bad request. Missing feature parameter.")

    def granted_actions(self):

        actuator_name = self.bodydict['actuator_name']
        value = self.bodydict['value']
        # TODO chimare metodo di claudio
        self.response.status_int = 200
        resp = json.dumps({"Actuator setted status": "OK"})
        self.response.write(resp)
