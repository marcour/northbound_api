import webapp2
import json
import logging
from google.appengine.ext import ndb

from handlers import RootHandler
from models import Configuration, Data
from datetime import datetime

import logging


class SensorHandler(RootHandler):

    def get_inputs(self):
        body = self.request.body
        self.bodydict = json.loads(body)

        self.mandatory = ["sensor_name",
                          "time_scope"]
        self.check_inputs()

        sensor_name = self.bodydict["sensor_name"]
        self.return_query = Configuration.query(
            Configuration.name == sensor_name).get()

    def granted_actions(self):
        """
        last
        now
        all
        range

        :return:
        """
        sensor = self.bodydict["sensor_name"]
        time_scope = self.bodydict["time_scope"]

        if self.bodydict["time_scope"] == "last":
            d = Data.query(Data.name == sensor).order(-Data.timestamp).get()
            resp = {"value": json.loads(d.value), "timestamp": datetime.strftime(d.timestamp, "%Y-%m-%d %H:%M:%S")}
            self.response.write(json.dumps({"entries": resp}, indent=4, sort_keys=True))


        elif self.bodydict["time_scope"] == "now":
            # TODO chiamare metodo self.claudio()
            pass

        elif "start_time" not in time_scope or "stop_time" not in time_scope:
            logging.warning("SensorHandler, granted_actions() method. Missing parameter start_time or stop_time")
            webapp2.abort(400,
                          "Bad request. Please check stop_time and start_time keys in time_scope field")
        else:
            # fai la query
            start_time = validate(time_scope["start_time"])
            stop_time = validate(time_scope["stop_time"])

            if start_time > stop_time:
                logging.warning("SensorHandler, granted_actions() method. Start time is after stop_time")
                webapp2.abort(400, "Bad request. Start time should be before stop_time.")

            data_list = Data.query(ndb.AND((Data.name == sensor),
                                   (Data.timestamp > start_time),
                                   (Data.timestamp < stop_time))).fetch()


            resp = [{"value": json.loads(d.value), "timestamp":datetime.strftime(d.timestamp, "%Y-%m-%d %H:%M:%S")} for d in data_list]
            self.response.write(json.dumps({"entries":resp}, indent=4, sort_keys=True))




def validate(date_text):
    try:
        date_dt = datetime.strptime(date_text, "%Y-%m-%d %H:%M:%S")
        return date_dt
    except Exception:
        logging.warning("SensorHandler, validate() method")
        webapp2.abort(400, "Bad request. Check timestamp format")
