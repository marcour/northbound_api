import json

import webapp2
from handlers import RootHandler
from models import Configuration
import logging

class BaseTriggerHandler(RootHandler):
    """
    Each TriggerHandler has to do the following steps:
    - override get_inputs()
    - call self.check_inputs() inside get_inputs
    - call self.get_conf() inside get_inputs after input check
    """

    def get_conf(self):
        if self.bodydict["event"] == "SENSOR":
            self.return_query = Configuration.query(
                Configuration.name == self.bodydict['feature']).get()
            self.conf_type = self.return_query.type
        else:
            # Only owner can access
            self.return_query = "not needed"
            self.conf_type = "friendship"

        friend_condition_space = ["FORMED"]
        sensor_codition_space = ["VERIFIED",
                                 "EQUAL",
                                 "NOT_EQUAL"]
        self.condition_space = []
        if self.conf_type == "number":
            self.condition_space = sensor_codition_space + ["GREATER_THAN",
                                                            "GREATER_THAN_OR_EQUAL",
                                                            "LESS_THAN",
                                                            "LESS_THAN_OR_EQUAL"]
        elif self.conf_type == "position":
            self.condition_space = sensor_codition_space + ["IN_RANGE",
                                                            "OUT_RANGE",
                                                            "INPUT_RANGE",
                                                            "OUTPUT_RANGE"]
        elif self.conf_type == "text":
            # RSSI_CHANGED and STATUS_CHANGED are not implemented yet
            self.condition_space = ["TECHNOLOGY_CHANGED",
                                    "RSSI_CHANGED",
                                    "STATUS_CHANGED"]

        elif self.conf_type == "friendship":
            self.condition_space = friend_condition_space

        else:
            logging.warning("BaseTriggerHandler, get_conf() method, no trigger for this type")
            webapp2.abort(400, "No trigger for this type")

        self.verify_condition_space()
        self.validate_inputs()

    def validate_inputs(self):

        if self.conf_type == "number":
            self.check_number()

        elif self.conf_type == "position":
            self.check_position()

        elif self.conf_type == "text":
            self.check_text()

        elif self.conf_type == "friendship":
            self.check_friendship()
        else:
            logging.warning("BaseTriggerHandler, validate_inputs() method, not in condition space")
            webapp2.abort(400, "Bad request, condition is not in condition space")

    def check_text(self):
            if self.bodydict["condition"] == "TECHNOLOGY_CHANGED":
                value_space = ["WIFI", "LTE", "UMTS", "EDGE"]
                if self.bodydict["value"].upper() not in value_space:
                    if self.bodydict["value"][0].upper() == "W":
                        webapp2.abort(400, "Bad value. Did you mean WIFI?")
                    webapp2.abort(400, "Bad value.")
            elif self.bodydict["condition"] == "RSSI_CHANGED" or self.bodydict["condition"] == "STATUS_CHANGED":
                # TODO implement RSSI_CHANGED and STATUS_CHANGED
                webapp2.abort(400, "Sorry, this condition type is not implemented yet.")

    def check_position(self):
        v = self.bodydict["value"]
        if "shape" not in v and "params" not in v:
            logging.warning("BaseTriggerHandler, check_position() method")
            webapp2.abort(400,
                          "Bad request. Please set value such as {'shape': 'your_shape', 'params': [...]}. Check documentation for more info")
        params = v["params"]
        if v["shape"].upper() == "CIRCLE":
            if "center" not in params and "radius" not in params:
                logging.warning("BaseTriggerHandler, check_position() method, missing circumference parameter")
                webapp2.abort(400, "Bad request. Please check center and radius keys")
            else:
                self.bodydict["value"] = str(params["center"][0])+","+str(params["center"][0])+";"+str(params["radius"])
        elif v["shape"].upper() == "POLYLINE":
            pts = ""
            if len(params) <= 2:
                logging.warning("BaseTriggerHandler, check_position() method, not enough points in polyline")
                webapp2.abort(400, "Bad request. Please insert at least two points")
            for point in params:
                # TODO check coordinates format
                if len(point) is not 2:
                    logging.warning("BaseTriggerHandler, check_position() method, bad point format")
                    webapp2.abort(400, "Bad request. Each point needs exactly two values")
                else:
                    pts += ";"+str(point[0])+","+str(point[1])
            self.bodydict["value"] = pts
    def check_number(self):
        try:
            float(self.bodydict["value"])
        except Exception:
            logging.warning("BaseTriggerHandler, check_number() method")

            webapp2.abort(400,
                          "Bad value format")

    def check_friendship(self):
        friendship_space = ["SOR", "POR", "OOR", "CLOR", "CWOR"]
        if self.bodydict["condition"] not in friendship_space:
            logging.warning("BaseTriggerHandler, check_friendship() method, not valid relationship")
            webapp2.abort(400, "Bad Request. Condition is not in condition space for this feature.")

    def verify_condition_space(self):
        condition = self.bodydict["condition"]
        if condition not in self.condition_space:
            logging.warning("BaseTriggerHandler, check_condition_space() method")
            webapp2.abort(400, "Bad Request. Condition is not in condition space for this feature.")


