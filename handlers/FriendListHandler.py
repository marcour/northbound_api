from datetime import datetime

import webapp2

from handlers import RootHandler
import json
from models import Friendship

class FriendListHandler(RootHandler):
    def get_inputs(self):
        body = self.request.body
        self.bodydict = json.loads(body)

        # only owner can access this handler
        self.return_query = "not needed"

    def granted_actions(self):
        try:
            rel = self.bodydict["friendship"]
        except Exception:
            rel = None

        if rel:
            friend_list = Friendship.query(Friendship.relation_type == rel).fetch()
        else:
            friend_list = Friendship.query().fetch()

        resp = [{"friend_key": friend.friend_key,
                 "timestamp": datetime.strftime(friend.timestamp,
                                                "%Y-%m-%d %H:%M:%S"),
                 "url": friend.url,
                 "relation_type": friend.relation_type,
                 "status": friend.status,
                 "id_vo": friend.id_VO}
                for friend in friend_list]

        self.response.write(
            json.dumps({"entries": resp}, indent=4, sort_keys=True))


