import json
from handlers.BaseTriggerHandler import BaseTriggerHandler
from models.Trigger import Trigger


class SetTriggerHandler(BaseTriggerHandler):

    def get_inputs(self):
        body = self.request.body
        self.bodydict = json.loads(body)

        self.mandatory = ["event",
                          "feature",
                          "condition",
                          "value",
                          "output_method",
                          "output_value",
                          "output_address",
                          "app_id",
                          "status"]

        self.check_inputs()
        self.get_conf()

    def granted_actions(self):
        event = self.bodydict['event']
        feature = self.bodydict['feature']
        value = self.bodydict['value']
        output_method = self.bodydict['output_method']
        output_value = self.bodydict['output_value']
        app_id = self.bodydict['app_id']
        status = self.bodydict['status']
        output_address = self.bodydict['output_address']

        trigger = Trigger()
        trigger.event = event
        trigger.feature = feature
        trigger.value = value
        trigger.output_method = output_method
        trigger.output_value = output_value
        trigger.appId = app_id
        trigger.status = status
        trigger.output_address = output_address
        trigger_id = trigger.put()
        # TODO impostare input per coordinate

        self.response.status_int = 200
        resp = json.dumps(
            {"trigger_id": trigger_id.id()})
        self.response.write(resp)
