import json

import webapp2
from google.appengine.ext.ndb import Key

from handlers import RootHandler
from handlers.BaseTriggerHandler import BaseTriggerHandler
from models.Trigger import Trigger


class StatusTriggerHandler(BaseTriggerHandler):
    def get_inputs(self):
        body = self.request.body
        self.bodydict = json.loads(body)
        try:
            _ = self.bodydict["trigger_id"]
            self.mandatory = []
        except:
            self.mandatory = ["event",
                              "feature",
                              "condition",
                              "value",
                              "output_method",
                              "output_value",
                              "output_address",
                              "app_id",
                              "status"]
            trigger = Trigger.query(Trigger.event == self.bodydict["event"] and
                                    Trigger.conditition == self.bodydict["condition"] and
                                    Trigger.output_method == self.bodydict["output_method"] and
                                    Trigger.output_value == self.bodydict["output_value"] and
                                    Trigger.appId == self.bodydict["app_id"] and
                                    Trigger.output_address == self.bodydict["output_address"] and
                                    Trigger.feature == self.bodydict["feature"]
                                    ).get()
            self.bodydict["trigger_id"] = trigger.key.id()

        # only owner can use triggerStatus
        self.return_query = "not needed"
        self.check_inputs()


    def granted_actions(self):
        trigger_id = self.bodydict['trigger_id']
        tr_status = self.bodydict['status']

        trigger = Key(Trigger, int(trigger_id)).get()
        trigger.status = tr_status
        trigger.put()

        self.response.status_int = 200
        resp = json.dumps(
            {"Trigger {} status".format(trigger_id): trigger.status})
        self.response.write(resp)
