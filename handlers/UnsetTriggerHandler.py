import json

import webapp2
from google.appengine.ext.ndb import Key
import logging

from handlers import RootHandler
from models.Trigger import Trigger


class UnsetTriggerHandler(RootHandler):
    def get_inputs(self):
        body = self.request.body
        self.bodydict = json.loads(body)

        try:
            trigger_id = self.bodydict["trigger_id"]
            self.mandatory = []
            self.bodydict["scope"] = "ONE"
        except Exception:
            self.mandatory = ["event",
                              "condition",
                              "value",
                              "output_method",
                              "output_value",
                              "app_id",
                              "scope",
                              "key"]
            try:
                if self.bodydict["scope"]=="ONE":
                    self.mandatory += ["feature"]
                elif self.bodydict["scope"]=="ALL":
                    self.mandatory = []
            except Exception:
                logging.warning("UnsetTriggerHandler, get_inputs() method. Missing scope parameter")
                webapp2.abort(400,
                              detail="Bad Request, missing parameter '{}'".format("scope"))

        # TODO unset trigger should be accessible public owner or friend
        self.return_query = "not needed"
        self.check_inputs()

    def granted_actions(self):
        is_all = False
        try:
            trigger_id = self.bodydict["trigger_id"]
            self.bodydict['scope'] = "ONE"
        except Exception:
            trigger_id = None

        if self.bodydict["scope"] == "ONE":

            if trigger_id is not None:
                trigger = Key(Trigger, int(trigger_id)).get()
                if trigger is not None:
                    trigger._entity_key.delete()
                else:
                    logging.warning("UnsetTriggerHandler, granted_actions() method. Trigger not found.")
                    webapp2.abort(410,detail="This trigger does not exists")

            else:
                trigger = Trigger.query(Trigger.event==self.bodydict["event"] and
                                        Trigger.conditition==self.bodydict["condition"] and
                                        Trigger.value==self.bodydict["value"] and
                                        Trigger.output_method==self.bodydict["output_method"] and
                                        Trigger.output_value==self.bodydict["output_value"] and
                                        Trigger.appId==self.bodydict["app_id"] and
                                        Trigger.output_address==self.bodydict["output_address"] and
                                        Trigger.feature==self.bodydict["feature"]
                                        ).get()

                if trigger is not None:
                    trigger._entity_key.delete()
                else:
                    logging.warning("UnsetTriggerHandler, granted_actions() method. Trigger not found.")
                    webapp2.abort(410,
                                  detail="This trigger does not exists")

        elif self.bodydict["scope"] == "ALL":
            is_all = True
            try:
                feature = self.bodydict["feature"]
            except Exception:
                feature = None

            if feature is not None:
                feature = self.bodydict["feature"]
                tr_list = Trigger.query(Trigger.feature == feature).fetch()
            else:
                tr_list = Trigger.query().fetch()

            for t in tr_list:
                t._entity_key.delete()


        else:
            logging.warning("UnsetTriggerHandler, granted_actions() method. Inconsistent scope.")
            webapp2.abort(400, detail="Bad Request, inconsistent scope")

        self.response.status_int = 200
        if is_all:
            resp = "ALL Trigger deleted"

        else:
            resp = json.dumps({"Trigger status": "deleted"})
        self.response.write(resp)
