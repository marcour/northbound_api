from google.appengine.api import memcache

from models import VoInfo


def get_grant_level(key):
    # TODO Usare la memcache per questa query

    my_owner_key = memcache.get('owner')
    my_friend_key = memcache.get('friend')
    if my_owner_key is None or my_friend_key is None:
        vo_info = VoInfo.query().get()
        my_owner_key = vo_info.owner_key
        my_friend_key = vo_info.friend_key
        memcache.add(key='owner', value=my_owner_key, time=3600)
        memcache.add(key='friend', value=my_friend_key, time=3600)


    grant = "denied"
    if my_owner_key == key:
        grant = "owner"
    elif my_friend_key == key:
        grant = "friend"

    return grant